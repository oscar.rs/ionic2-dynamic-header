import { Component, ViewChild, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'dynamic-header',
    templateUrl: 'dynamic-header.html'
})
export class DynamicHeader implements OnInit {



    @Output() returnBack = new EventEmitter<boolean>();
    /**
     * Background Image this is required
     */
    @Input('image') image;
    /**
     * The content the page this must be inside the a tag <ion-content>
     * It's required
     */
    @Input('content') content;
    /**
     * Title the header (optional)
     */
    @Input('title') title;
    /**
     * Subtitle the header (optional)
     */
    @Input('subtitle') subtitle;
    /**
     * Icon or small image the title (optional)
     */
    @Input('imgRef') imgRef; 
    /**
     * Size the width the viewport (optional)
     */
    public viewPortWidth;
    /**
     * Flag to get if is the first time change header.
     */
    public firstTime;
    /**
     * Flag for change header the absolute to fixed position.
     */
    public showFixedHeader;

    public arrowTop: string;


    /**
     * For control the header opacity
     */
    @ViewChild('header') header;

    constructor(private navCtrl: NavController){

    }

    ngOnInit(): void {
        this.viewPortWidth = this.header.nativeElement.firstElementChild.offsetWidth;
        this.arrowTop = ((this.viewPortWidth * 25) /100) - 70 + 'px';
        this.header.nativeElement.firstElementChild.style.height = this.header.nativeElement.firstElementChild.offsetWidth + 'px';
        this.content._elementRef.nativeElement.lastChild.addEventListener('scroll', evt => {
            if (this.header){
                this.header.nativeElement.firstElementChild.style.opacity = 1 - (evt.target.scrollTop/200);
                this.header.nativeElement.firstElementChild.style.height = this.header.nativeElement.firstElementChild.offsetWidth + 'px';
            }
            if ( (this.viewPortWidth - evt.target.scrollTop) <= (this.viewPortWidth * 25) /100 ){
                this.showFixedHeader = true;
                this.firstTime = true;
                
            }else{
                this.showFixedHeader = false;
            }


            // METHOD WHEN SCROLL FINISH IN BOTTOM
            if ( evt.target.scrollTop + evt.target.clientHeight >= evt.target.scrollHeight){
            }
        })
    }
    /**
     * 
     * Method for return prev page;
     */
    returnPrevPage(): void{
        this.returnBack.emit(true);
        // this.navCtrl.pop();
    }

}
